weblogs
=======

Fork from github.com/keep94/weblogs.

## Installing

	go get bitbucket.org/coinpass/weblogs
	goven bitbucket.org/coinpass/weblogs

## Using

	import "bitbucket.org/coinpass/weblogs"

## Features

If server panics before sending a response, weblogs automatically sends a
500 error to client and logs the panic.

## Online Documentation

Online documentation available [here](http://godoc.org/github.com/keep94/weblogs).

## Example Usage

	var port := 8080

	http.ListenAndServe(":"+port, weblogs.Handler(http.DefaultServeMux))

## Using ``context`` to add key/values

Import this package into the file when you are storing your key/value

	import "bitbucket.org/coinpass/weblogs"

Invoke the values container and set your key

	v := weblogs.Values(req)
	v["test"] = "IT WORKS!"

To retrieve and display the value, just invoke it in your Log function this way

	func (l myLogger) Log(w io.Writer, log *LogRecord) {

		// ... //

		fmt.Fprintf(w, "info:  date=%s, method=%s, url=%s, params=%v, "+

			// ... //

			log.Values,

			// ... //
		)
	}
